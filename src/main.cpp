#include <Arduino.h>
#include <TinyGPSPlus.h>
#include "Wire.h"

#define RXD2 16
#define TXD2 17

int SATELLITES=0;

// The TinyGPSPlus object
TinyGPSPlus gps;

void displayInfo();


void setup() {
  Serial.begin(115200);
  
  //Begin serial communication Neo6mGPS
  Serial2.begin(9600, SERIAL_8N1, RXD2, TXD2);
  //Serial2.setPins(16,17);
  delay(3000);
}
 

void loop() {

  while (Serial2.available() > 0){

    if (gps.encode(Serial2.read())){

      if(gps.satellites.isValid() && gps.satellites.isUpdated()){

        SATELLITES=gps.satellites.value();
        
        Serial.print("Satellites: ");

        Serial.println(SATELLITES);
      }
      
      //if (SATELLITES<=12 && SATELLITES>=4){

      displayInfo();

      //}
     
    }

  }


  if (millis() > 5000 && gps.charsProcessed() < 10)
  {

    Serial.println(F("No GPS detected: check wiring."));

    while(true);

  }

}

void displayInfo()
{
  Serial.print(F("Location: ")); 
  if (gps.location.isValid())
  {
    Serial.print(gps.location.lat(), 6);
    Serial.print(F(","));
    Serial.print(gps.location.lng(), 6);
  }
  else
  {
    Serial.print(F("INVALID"));
    Serial.print(gps.location.lat(), 6);
    Serial.print(F(","));
    Serial.print(gps.location.lng(), 6);
  }

  Serial.print(F("  Date/Time: "));
  if (gps.date.isValid())
  {
    Serial.print(gps.date.month());
    Serial.print(F("/"));
    Serial.print(gps.date.day());
    Serial.print(F("/"));
    Serial.print(gps.date.year());
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.print(F(" "));
  if (gps.time.isValid())
  {
    if (gps.time.hour() < 10) Serial.print(F("0"));
    Serial.print(gps.time.hour());
    Serial.print(F(":"));
    if (gps.time.minute() < 10) Serial.print(F("0"));
    Serial.print(gps.time.minute());
    Serial.print(F(":"));
    if (gps.time.second() < 10) Serial.print(F("0"));
    Serial.print(gps.time.second());
    Serial.print(F("."));
    if (gps.time.centisecond() < 10) Serial.print(F("0"));
    Serial.print(gps.time.centisecond());
  }
  else
  {
    Serial.print(F("INVALID"));
  }

  Serial.println();
}